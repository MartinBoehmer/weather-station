#!/bin/bash

# URL to be open in the browser
openhab_url="http://localhost:8080/habpanel/index.html#/view/clock"
# Number of seconds after that openHAB should be up and running
openhab_start_timeout=60

script_dir=$( dirname "$0" )

# Check if openHAB is up
for try in $(seq 1 $openhab_start_timeout)
#for try in {1..10}
do
  openhab_response=$( curl --write-out %{http_code} --silent --output /dev/null --head "$openhab_url" )
  #echo "Try: $try, response: $openhab_response"
  if [ "$openhab_response" == "200" ]
  then
    break
  fi
  sleep 1
done

# Chromium auto-start
chromium-browser --kiosk "$openhab_url"
