# openHAB

<a href="habpanel-preview.PNG"><img src="habpanel-preview.PNG" align="right" width="200" ></a>

In this step, the popular home automation software [openHAB](https://www.openhab.org/docs/) is installed and basically setup for further steps.

[[_TOC_]]

## Prerequisites

### Required parts

No other parts required than those from the [preparation step](../step00-preparation/).

### Steps to be completed prior to this step

* [Preparation](../step00-preparation/)

## Software installation

### Java embedded

Add Zulu archives for Debian to the APT repository. For details see [Zulu documentation](https://docs.azul.com/zulu/zuludocs/ZuluUserGuide/PrepareZuluPlatform/AttachAPTRepositoryUbuntuOrDebianSys.htm).

```bash
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 0xB1998361219BD9C9
echo 'deb http://repos.azulsystems.com/debian stable main' | sudo tee /etc/apt/sources.list.d/zulu.list
```

Install Zulu Embedded Java 8, [according to Zulu documentation](https://docs.azul.com/zulu/zuludocs/ZuluUserGuide/InstallingZulu/InstallOnLinuxUsingAPTRepository.htm):

```bash
sudo apt-get update
sudo apt-get install zulu-embedded-8
```

Check, if Java works:

```
java -version
```

Example output:

```bash
openjdk version "1.8.0_152"
OpenJDK Runtime Environment (Zulu Embedded 8.25.0.76-linux-aarch32hf) (build 1.8.0_152-b76)
OpenJDK Client VM (Zulu Embedded 8.25.0.76-linux-aarch32hf) (build 25.152-b76, mixed mode, Evaluation)
```

### openHAB

Install openHAB2 and add-ons via the [package repository method](https://www.openhab.org/docs/installation/linux.html#package-repository-installation):

```bash
sudo apt-get install apt-transport-https
wget -qO - 'https://bintray.com/user/downloadSubjectPublicKey?username=openhab' | sudo apt-key add -
echo 'deb https://dl.bintray.com/openhab/apt-repo2 stable main' | sudo tee /etc/apt/sources.list.d/openhab2.list
sudo apt-get update
sudo apt-get install openhab2 openhab2-addons
```

Activate openHAB on system startup and start openHAB:

```
sudo systemctl enable openhab2
sudo service openhab2 start
```

Check if openHAB is running by directing your browser to http://mypi:8080. The configuration of openHAB is continued in a [later section](#openhab-configuration).

### Nginx web-server

The [Nginx](https://nginx.org/en/docs/) web-server is used to access openHAB from other devices and provide access to the openHAB configuration via WebDAV. In a [later step](../stepXX/), it is also used to secure the communication with these devices.

#### Basic setup

```bash
sudo apt-get install nginx
```

Setup a site as proxy for OpenHAB by creating the following file:

```bash
sudo nano /etc/nginx/sites-available/openhab
```

Fill in the following content. You need to replace the host names ```mypi``` and ```mypi.myhome``` by your Pi's host name(s) and/or IP, e. g. "openhab".

```
server {
    listen 80;
    listen [::]:80;

    server_name mypi mypi.myhome;

    location / {
        proxy_pass http://localhost:8080/;
        include /etc/nginx/proxy_params;
    }
}
```

Enable the site:

```bash
sudo ln -s /etc/nginx/sites-available/openhab /etc/nginx/sites-enabled/
sudo nginx -t
sudo systemctl restart nginx
```

Now is a good time to check, if you can access openHAB via http://mypi/ with your browser. The configuration of openHAB is continued in a [later section](#openhab-configuration).

Setup the firewall to allow accessing openHAB only via Nginx. Allowing port 3389 is only required, if remote desktop access has been setup in the [preparation step](../step00-preparation/).

```bash
sudo apt-get install ufw
sudo ufw allow OpenSSH
sudo ufw allow 3389
sudo ufw allow "Nginx Full"
sudo ufw enable
```

## openHAB configuration

It is important to understand OpenHAB's approach to configuration to avoid confusion and frustration. There are several ways to configure openHAB that can be used all at the same time and merge to an effective configuration:

* [PaperUI](https://www.openhab.org/docs/configuration/paperui.html)
A GUI that can be installed as an add-on. It does not cover 100% of the configuration options and tasks yet. The settings are stored in ```/var/lib/openhab2``` and spread across several files and formats.
* File-based configuration
The configuration files are located in ```/etc/openhab2```. They cover the entirety of configuration options and tasks. OpenHAB does not change these files, but only reads them, so they are maintained manually. On the Pi, a text editors like Nano can be used to edit them.

As said, you may use both ways at the same time, but I recommend to go with configuration files and use PaperUI only for exploration and testing.

### Remote access to configuration files

There a several tools that support editing the configuration files, e. g. by code highlighting and auto completion. A popular one is [Visual Studio Code](https://code.visualstudio.com/) as it run on Windows, Mac and Linux. Considering that there are plenty of (video) tutorials on how to setup and use Visual Studio Code, this guide focuses on setting up the remote access to the openHAB configuration files, which is required to edit them remotely via Visual Studio Code or other tools.

A range of alternatives qualifies for realising the remote access:

* Use **rsync**, **scp** or *git* to transfer the files from/to the Pi. Major disadvantage: Syncing or copying is required after every change.
* Share files using **WebDAV** provided by [Nginx WebDAV module](http://nginx.org/en/docs/http/ngx_http_dav_module.html) (as Nginx has already been installed). Major disadvantage: I failed to make this work with a Windows client.
* Share files using [**Samba**](). Major disadvantage: none.

Based on the above evaluation, this guide goes with **Samba**. Check out [this tutorial](https://pimylifeup.com/raspberry-pi-samba/) for further details.

Install the Samba service and modify the firewall to allow external connections to it. If you're asked whether to use the WINS server provided by your DHCP server, answer "no" unless you exactly know what this means (if you need this, you will know).

```bash
sudo apt-get install samba samba-common-bin
sudo ufw allow Samba
```

A Samba account requires a matching account on the the Pi's Raspbian to be present and explicit enabled for using Samba.

The openHAB configuration files must be readable and, more importantly, writeable to the Samba users. As openHAB does not write to these files itself, we can safely change the ownership as long as we ensure the ```openhab``` system user belongs to the group that owns the files.

```bash
sudo addgroup --system openhab-config
sudo usermod -aG openhab-config openhab
sudo usermod -aG openhab-config pi
sudo chown -R root:openhab-config /etc/openhab2
sudo chmod -R g+w /etc/openhab2
```

By default, [Samba passwords are automatically synced](https://askubuntu.com/questions/153893/samba-and-user-acount-passwords) with the Raspbian login passwords.

So, we just need to initially enable the Pi's default user ```pi``` for Samba. As we must provide a password here, use the same as the system password (future password changes will be synched automatically, see above):

```bash
sudo smbpasswd -a pi
```

You may also create a new system user and enable it in addition to or instead of the ```pi``` user. See this [guide for creating users](https://linuxize.com/post/how-to-create-users-in-linux-using-the-useradd-command/). If you think about using the ```òpenhab``` system user, this will not work as Samba does not permit login of system users (uid < 1000).

Create a new network share exposing the configuration files by opening the Samba configuration:

```bash
sudo nano /etc/samba/smb.conf
```

At the end the file, add the following lines. Make sure you change or remove the ```valid users``` setting according to your setup.

```
[openhab-config]
path = /etc/openhab2
writeable = yes
browseable = yes
create mask = 0664
force create mode = 0664
directory mask = 0775
force directory mode = 0775
force group = openhab-config
public = no
valid users = pi
```

To improve the compatibility of windows clients, add the following somewhere under the ```[global]``` section and before the share definitions start:

```
#==================== Custom global settings =====================

ntlm auth = true
```

Save and close the Samba configuration.

Finally, restart Samba:

```bash
sudo service smbd restart
```

### Basic configuration

#### Alternative A: File-based configuration (preferred)

Open openHAB with your browser as previously setup, e. g. via http://mypi/.

You should now be prompted to choose a package for the initial setup as [outlined in the documentation](https://www.openhab.org/docs/tutorial/1sttimesetup.html). Choose **Skip the package selection** as we will do that manually.

For editing the configuration files, we stick to working on the Pi with Nano, but you may also edit them remotely using e. g. Visual Studio Code (see above).

Open the add-ons configuration file:

```bash
sudo nano /etc/openhab2/services/addons.cfg
```

Uncomment and set the following settings:

```
ui = basic, habpanel
persistence = mapdb
```

Open the runtime configuration file:

```bash
sudo nano /etc/openhab2/services/runtime.cfg
```

Uncomment or add the following settings and set them for your region:

```
org.eclipse.smarthome.i18n:language=de
org.eclipse.smarthome.i18n:region=DE
org.eclipse.smarthome.i18n:timezone=Europe/Berlin
org.eclipse.smarthome.persistence:default=mapdb
```

#### Alternative B: PaperUI-based configuration

Open openHAB with your browser as previously setup, e. g. via http://mypi/.

You should now be prompted to choose a package for the initial setup as [outlined in the documentation](https://www.openhab.org/docs/tutorial/1sttimesetup.html). Choose **Standard Setup**.

When done, open Paper UI to perform and the following tasks:

- Configuration
   - Addons
      - Install the [MapDB binding](https://www.openhab.org/addons/persistence/mapdb/) under the "persistence" tab
   - System
      - Regional settings
         - Set your language, country and time zone
         - Click "more" and select your measurement system (e. g. metric)
      - Persistence
         - Set the default service for persistence to MapDB
- Preferences
   - Set your preferred UI language

### Simple clock

#### Setup

Navigate your browser to HabPanel, e. g. http://mypi/habpanel/. Click in the cog icon (upper right corner) and then on "panel settings" (on the right side).

Click "edit local panel configuration (experts only)" and replace the entire content by importing the following file: [habpanel-simple-clock.json](habpanel-simple-clock.json)

Click save. Back in the settings screen, you should **save the local configuration to a new panel configuration**, in case you want to use the panel on another device than the one you are working with right now.

Finally, you can open the "Clock" panel and enjoy watching time fly by...

#### Auto-start configuration

In order to automatically show the clock when the Pi starts, some additional steps have to be taken.

To hide the mouse pointer, we need unclutter:

```bash
sudo apt-get install unclutter
```

Clone this repository to your Pi as it includes the script to start the web-browser on the Pi:

```bash
cd ~
git clone https://gitlab.com/MartinBoehmer/weather-station.git
```

Now we need to configure the auto-start of the clock panel.

```bash
sudo nano /etc/xdg/lxsession/LXDE-pi/autostart
```

The contents of the file should look as follows:

```
@lxpanel --profile LXDE-pi
@pcmanfm --desktop --profile LXDE-pi
# Disable screensaver
#@xscreensaver -no-splash

# Disable power saving and blank screen
@xset s off
@xset -dpms
@xset s noblank

# Hide mouse pointer
@unclutter

# Start chromium in kiosk mode
@/home/pi/weather-station/step02-openhab/chromium_start.sh
```

For more details and troubleshooting on auto-start in Raspbian, see [this website](https://www.raspberrypi-spy.co.uk/2014/05/how-to-autostart-apps-in-rasbian-lxde-desktop/).
