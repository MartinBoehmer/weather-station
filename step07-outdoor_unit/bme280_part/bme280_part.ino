#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>

// Interval to read sensor data in milliseconds
#define READ_INTERVAL 1000

// BME280 settings
#define SEALEVELPRESSURE_HPA (1013.25)

// The sensor object
Adafruit_BME280 bme;

void setupBme280(void) {
  if (bme.begin(BME280_ADDRESS_ALTERNATE)) {
    Serial.println(F("Found a BME280 sensor!"));
  } else {
    Serial.println(F("No BME280 sensor found!"));
    while (1);
  }
}

void printBme280(void) {
  float bmeTempC = bme.readTemperature();
  float bmePressureHpa = bme.readPressure() / 100.0F;
  float bmeHumidity = bme.readHumidity();
  float bmeAltM = bme.readAltitude(SEALEVELPRESSURE_HPA);
  Serial.print(F("Read BME280: "));
  Serial.print(F("Temperature [C] = ")); Serial.print(bmeTempC);
  Serial.print(F(", Pressure [hPa] = "));  Serial.print(bmePressureHpa);
  Serial.print(F(", Humidity [%] = ")); Serial.print(bmeHumidity);
  Serial.print(F(", Approx. altitude [m] = ")); Serial.print(bmeAltM);
  Serial.println();
}

void setupSerial(void) {
  Serial.begin(115200);
  // Wait for Serial to be initialised
  while (!Serial);
}

void setup() {
  setupSerial();
  setupBme280();
}

void loop() {
  printBme280();
  delay(READ_INTERVAL);
}
