#include <WiFi.h>
#include <WiFiClientSecure.h>
#include <PubSubClient.h>
#include "outdoor_unit_config.h"

// Global objects
WiFiClientSecure wifiClient;
PubSubClient mqttClient(mqtt_broker_host, mqtt_broker_port, wifiClient);

void setupSerial(void) {
  Serial.begin(115200);
  // Wait for Serial to be initialised
  while (!Serial);
}

void setup() {
  setupSerial();
  setupTsl2591();
  setupBme280();
  setupWiFiClient();
  connectMqttClient();
}

void loop() {
  printBme280();
  printTsl2591();
  publishBme280();
  publishTsl2591();
  mqttClient.loop();
  delay(read_interval);
}
