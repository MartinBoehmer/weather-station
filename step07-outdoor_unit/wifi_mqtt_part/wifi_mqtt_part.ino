#include <WiFi.h>
#include <WiFiClientSecure.h>
#include <PubSubClient.h>
#include "wifi_mqtt_config.h"

// Configuration: see seperate header file

// Status LED that is turned on in case of connection errors
#define ESP_LED_PIN 2

// MQTT objects
WiFiClientSecure wifiClient;
PubSubClient mqttClient(mqtt_broker_host, mqtt_broker_port, wifiClient);

void setupWiFiClient(void) {
  // Setup LED: off = no errors
  pinMode(ESP_LED_PIN, OUTPUT);
  digitalWrite(ESP_LED_PIN, LOW);
  // WiFi
  Serial.print(F("Connecting to WiFi: "));
  Serial.print(wifi_ssid);
  WiFi.mode(WIFI_STA); // required for ESP boards, see https://github.com/knolleary/pubsubclient/issues/138#issuecomment-326113915
  WiFi.begin(wifi_ssid, wifi_pwd);
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(F("."));
    // Status LED blinks while connecting
    digitalWrite(ESP_LED_PIN, HIGH);
    delay(1000);
    digitalWrite(ESP_LED_PIN, LOW);
    delay(1000);
    digitalWrite(ESP_LED_PIN, HIGH);
    delay(1000);
    digitalWrite(ESP_LED_PIN, LOW);
    delay(1000);
  }
  Serial.println();
  Serial.print(F("WiFi connected, IP address: "));
  Serial.println(WiFi.localIP());
  // Add CA cert to securely connect to MQTT broker
  wifiClient.setCACert(ca_cert);
}

void connectMqttClient(void) {
  while (!mqttClient.connected()) {
    if (mqttClient.connect(mqtt_client_id, mqtt_client_user, mqtt_client_pwd)) {
      Serial.print(F("Connected to MQTT broker: "));
      Serial.println(mqtt_broker_host);
      digitalWrite(ESP_LED_PIN, LOW);
    } else {
      Serial.print(F("Failed to connect to MQTT broker: host = "));
      Serial.print(mqtt_broker_host);
      Serial.print(F(", MQTT client state = "));
      Serial.print(mqttClient.state());
      Serial.print(F(", WiFi status = "));
      Serial.print(WiFi.status());
      Serial.println();
      digitalWrite(ESP_LED_PIN, HIGH);
      delay(5000);
    }
  }
}

void publishValue(const char* topic, float value, int precision) {
  char valueAsChar[10];
  dtostrf(value, 1, precision, valueAsChar);
  publishValue(topic, valueAsChar);
}

void publishValue(const char* topic, const char* message) {
  connectMqttClient();
  Serial.print(F("Publishing to "));
  Serial.print(topic);
  if (mqttClient.publish(topic, message)) {
    Serial.println(F(" - Done"));
  } else {
    Serial.println(F(" - Failed!"));
    Serial.print(F("MQTT client state = "));
    Serial.println(mqttClient.state());
  }
}

void setupSerial(void) {
  Serial.begin(115200);
  // Wait for Serial to be initialised
  while (!Serial);
}

void setup() {
  setupSerial();
  setupWiFiClient();
  connectMqttClient();
}

void loop() {
  publishValue("/test", "Hello wordl!");
  mqttClient.loop();
  delay(READ_INTERVAL);
}
