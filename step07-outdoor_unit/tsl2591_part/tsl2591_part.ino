#include <Wire.h>
#include <Adafruit_Sensor.h>
#include "Adafruit_TSL2591.h"

// Interval to read sensor data in milliseconds
#define READ_INTERVAL 1000

// The sensor object
Adafruit_TSL2591 tsl = Adafruit_TSL2591(0);

void setupTsl2591(void) {
  tsl.setGain(TSL2591_GAIN_MED);
  tsl.setTiming(TSL2591_INTEGRATIONTIME_300MS);
  if (tsl.begin()) {
    Serial.println(F("Found a TSL2591 sensor"));
  } else {
    Serial.println(F("No TSL2591 sensor found!"));
    while (1);
  }
}

void printTsl2591(void) {
  uint16_t tslVisible = tsl.getLuminosity(TSL2591_VISIBLE);
  uint16_t tslFull = tsl.getLuminosity(TSL2591_FULLSPECTRUM);
  uint16_t tslIr = tsl.getLuminosity(TSL2591_INFRARED);
  uint16_t tslLux = tsl.calculateLux(tslFull, tslIr);
  Serial.print(F("Read TSL2591: "));
  Serial.print(F("Full = ")); Serial.print(tslFull);
  Serial.print(F(", IR = "));  Serial.print(tslIr);
  Serial.print(F(", Visible = ")); Serial.print(tslVisible);
  Serial.print(F(", Lux = ")); Serial.print(tslLux);
  Serial.println();
}

void setupSerial(void) {
  Serial.begin(115200);
  // Wait for Serial to be initialised
  while (!Serial);
}

void setup() {
  setupSerial();
  setupTsl2591();
}

void loop() {
  printTsl2591();
  delay(READ_INTERVAL);
}
