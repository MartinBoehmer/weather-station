# Outdoor sensor unit

<a href="outdoor_unit_breadboard_front.jpg"><img src="outdoor_unit_breadboard_front.jpg" align="right" width="200" ></a>

This step implements an outdoor unit that measures temperature, humidity, air pressure as well as illuminance and sends the data to openHAB. It is based on an ESP32 microcontroller.

[[_TOC_]]

## Prerequisites

### Required parts

* [Developer board](https://makeradvisor.com/tools/esp32-dev-board-wi-fi-bluetooth/) based on ESP-WROOM 32 microcontroller. Here, the [SBC-NodeMCU-ESP32 from JoyIT](https://joy-it.net/en/products/SBC-NodeMCU-ESP32) is used.
* BME280 sensor (temperature, humidity, air pressure)
* TLS2591 sensor (illuminance)
* Previously used parts from [preparation](../step00-preparation/) step

### Steps to be completed prior to this step

* [Preparation](../step00-preparation/)
* [openHAB](../step02-openhab/)
* [Indoor temperature and humidity](../step03-indoor-temp/)
* [Security](../step06-security)

## Get the ESP32 going

First, the development environment for programming the microcontroller must be setup. Basically, there are several options:

* [Arduino programming language](https://www.arduino.cc/reference/en/) using the [Arduino IDE (Windows, Windows Store, Mac, Linux)](https://www.arduino.cc/en/Main/Software) and 
* [MicroPython](https://micropython.org/) or [CirciutPython](https://circuitpython.org/) with the IDE of your choice, e. g. a text editor, [Mu](https://learn.adafruit.com/welcome-to-circuitpython/installing-mu-editor), [Visual Studio Code](https://lemariva.com/blog/2018/12/micropython-visual-studio-code-as-ide), [PyCharm](https://plugins.jetbrains.com/plugin/9777-micropython), [uPyCraft](https://github.com/DFRobot/uPyCraft), etc.
* Plain C/C++ using e. g. [Eclipse IDE](https://www.instructables.com/id/ESP32-With-Eclipse-IDE/)

This guide will go with the Arduino option.

### Install Arduino IDE

Download, install and start the [Arduino IDE](https://www.arduino.cc/en/Main/Software).

The the ESP32 board manager needs to be added. Copy the __stable release link__ from [this GitHub repository](https://github.com/espressif/arduino-esp32/blob/master/docs/arduino-ide/boards_manager.md) and paste it into the Arduino IDE under __File__ -> __Preferences__ -> __Additional board manager URLs__. Then go to __Tools__ -> __Board__ -> __Board Manager__, search for "esp32" and install the package. See [this video tutorial](https://youtu.be/xPlN_Tk3VLQ?t=572) for screenshots and more details about this procedure.

### Connect the ESP32

Connect the ESP32 to your computer or Raspberry Pi using a USB cable. Make sure it is a proper data cable, not a cable for charging only.

In the Arduino IDE select __Tools__ -> __Port__ -> whatever port is available, e. g. COM3

### Run an example program on the ESP32

The ESP32 board manager comes with lots of examples. Load this one: __File__ -> __Examples__ -> __ESP32__ -> __ChipID__ -> __GetChipID__.

Upload the program (__Sketch__ -> __Upload__) and watch the output in the log window at the bottom. You may need to press or press and hold on of the buttons on your ESP32 depending on your model. Consult your ESP32's manual, if ion doubt.

Open the serial monitor (__Tools__ -> __Serial monitor__) and watch the ESP32 printing its chip id.

Example serial console output:

```
ESP32 Chip ID = 12A34CC789F0
ESP32 Chip ID = 12A34CC789F0
ESP32 Chip ID = 12A34CC789F0
...
```

## Wire and read the BME280 sensor

### Wire

Wiring the sensor is simple as it can be directly connected to the ESP32. The pin numbers on the ESP32 reflect the numbering according to the [official documentation of the used board](https://joy-it.net/files/files/Produkte/SBC-NodeMCU-ESP32/SBC-NodeMCU-ESP32-Manual-20200320.pdf).

| Sensor pin (function) | ESP32 pin (function) |
| ---                   | ---                  |
| VCC                   | 3.3V (power)         |
| GND                   | GND                  |
| SCL (I2C clock)       | D22 (I2C-SCL)        |
| SDA (I2C data)        | D21 (I2C-SDA)        |
| CSB (unused)          | -                    |
| SDO (unused)          | -                    |

### Prepare Arduino IDE

The library for the sensor needs to be made available to the Arduino IDE.

Select __Tools__ -> __Manage Libraries...__. Type ```bm280``` in the search box and install the [**Adafruit BME280 library**](https://github.com/adafruit/Adafruit_BME280_Library) including all dependencies (latest version at time of writing 2.1.0 is used here).

Now, the ESP32 can be programmed to read the sensor data. First, we will use an example sketch for testing and then create a sketch specific to the outdoor unit.

### Test

Load the example provided with the library: __File__ -> __Examples__ -> __Adafruit BME2591 Library__ -> __bme280test__.

Because the SDO pin is not connected, the sensor's I2C address is 0x76 (otherwise 0x77). As the library's default address is 0x77, this needs to be changed in the sketch. Find the following line (around line 45):

```
    status = bme.begin();
```

... and type in the sensors actual I2C address, so the line looks like this:

```
    status = bme.begin(0x76);
```

Open a serial monitor (__Tools__ -> __Serial Monitor__) and set it to 9600 baud.
Upload the program (__Sketch__ -> __Upload__) and watch the output in the log window at the bottom. Once the program starts, you should see the following output on the serial monitor:

```
BME280 test
-- Default Test --

Temperature = 26.79 *C
Pressure = 991.07 hPa
Approx. Altitude = 186.36 m
Humidity = 59.16 %

...
```

#### Troubleshooting

In case things do not work out as described above:

* double-check the wiring (visually)
* if you own a multimeter, perform a continuity check on the wiring (disconnect the ESP32 from power first!)
* use an [I2C scanner sketch](https://raw.githubusercontent.com/RuiSantosdotme/Random-Nerd-Tutorials/master/Projects/LCD_I2C/I2C_Scanner.ino) to check if sensors are recognised.
* read [this tutorial](https://randomnerdtutorials.com/esp32-i2c-communication-arduino-ide/) to learn more about I2C

### Program

The sketch to implement reading the BME280 sensor is provided with this guide: [bme280_part.ino](bme280_part/bme280_part.ino).

It essentially consists of three functions to initialise the Serial Monitor, initialise the sensor and print the sensor data. These functions will later be copied and pasted into the complete sketch for the outdoor unit.

Open a serial monitor (__Tools__ -> __Serial Monitor__) and set it to 115200 baud.
Upload the program (__Sketch__ -> __Upload__) and watch the output in the log window at the bottom. Once the program starts, you should see the following (example) output on the serial monitor:

```
Found a BME280 sensor!
Read BME280: Temperature [C] = 27.29, Pressure [hPa] = 993.75, Humidity [%] = 49.70, Approx. altitude [m] = 163.61
Read BME280: Temperature [C] = 27.31, Pressure [hPa] = 993.75, Humidity [%] = 49.94, Approx. altitude [m] = 163.63
...
```

## Wire and read the TLS2591 sensor

### Wire

Wiring the sensor is simple as it can be directly connected to the ESP32. The pin numbers on the ESP32 reflect the numbering according to the [official documentation of the used board](https://joy-it.net/files/files/Produkte/SBC-NodeMCU-ESP32/SBC-NodeMCU-ESP32-Manual-20200320.pdf).

| Sensor pin (function) | ESP32 pin (function) |
| ---                   | ---                  |
| VIN                   | 3.3V (power)         |
| GND                   | GND                  |
| 3vo (unused)          | -                    |
| Int (unused)          | -                    |
| SDA (I2C data)        | D21 (I2C-SDA)        |
| SDC (I2C clock)       | D22 (I2C-SCL)        |

### Prepare Arduino IDE

The library for the sensor needs to be made available to the Arduino IDE.

Select __Tools__ -> __Manage Libraries...__. Type ```tsl2591``` in the search box and install the [**Adafruit TSL2591 library**](https://github.com/adafruit/Adafruit_TSL2591_Library) including all dependencies (latest version at time of writing 1.2.1 is used here).

Now, the ESP32 can be programmed to read the sensor data. First, we will use an example sketch for testing and then create a sketch specific to the outdoor unit.

### Test

Load the example provided with the library: __File__ -> __Examples__ -> __Adafruit TSL2591 Library__ -> __tsl2591__.

Open a serial monitor (__Tools__ -> __Serial Monitor__) and set it to 9600 baud.
Upload the program (__Sketch__ -> __Upload__) and watch the output in the log window at the bottom. Once the program starts, you should see the following output on the serial monitor:

```
Starting Adafruit TSL2591 Test!
Found a TSL2591 sensor
------------------------------------
Sensor:       TSL2591
Driver Ver:   1
Unique ID:    2591
Max Value:    88000.00 lux
Min Value:    0.00 lux
Resolution:   0.0010 lux
------------------------------------

------------------------------------
Gain:         25x (Medium)
Timing:       300 ms
------------------------------------

[ 1066 ms ] IR: 2305  Full: 6339  Visible: 4034  Lux: 139.652908
[ 1927 ms ] IR: 2331  Full: 6403  Visible: 4072  Lux: 140.874023
[ 2788 ms ] IR: 2371  Full: 6500  Visible: 4129  Lux: 142.684021
[ 3649 ms ] IR: 2415  Full: 6607  Visible: 4192  Lux: 144.689545
...
```

#### Troubleshooting

See troubleshooting for BME280.

### Program

The sketch to implement reading the TSL2591 sensor is provided with this guide: [tsl2591_part.ino](tsl2591_part/tsl2591_part.ino).

It essentially consists of three functions to initialise the Serial Monitor, initialise the sensor and print the sensor data. These functions will later be copied and pasted into the complete sketch for the outdoor unit.

Open a serial monitor (_Tools__ -> __Serial Monitor__) and set it to 115200 baud.
Upload the program (__Sketch__ -> __Upload__) and watch the output in the log window at the bottom. Once the program starts, you should see the following (example) output on the serial monitor:

```
Found a TSL2591 sensor
Read TSL2591: Full = 7821, IR = 2781, Visible = 3813, Lux = 176
Read TSL2591: Full = 7888, IR = 2849, Visible = 4953, Lux = 175
...
```

## Send the sensor data to the MQTT Broker

### Prepare Arduino IDE

The library for MQTT support needs to be made available to the Arduino IDE.
Select __Tools__ -> __Manage Libraries...__. Type ```pubsub``` in the search box and install the [**PubSubClient**](https://github.com/knolleary/pubsubclient) including all dependencies (latest version at time of writing 2.8.0 used here).

Now, the ESP32 can be programmed to talk to an MQTT broker.

### Program

The sketch to implement connecting to the WiFi and MQTT broker is provided with this guide: [wifi_mqtt_part.ino](wifi_mqtt_part/wifi_mqtt_part.ino).

#### Configuration

You need to adjust the configuration, before you can run the sketch. The sketch configuration is placed in a separate header file in the sketch folder.

Copy the provided [configuration example](wifi_mqtt_part/wifi_mqtt_config.h.example) and name it ```wifi_mqtt_confiog.h```.

As most of the settings are straightforward, let's focus on adjusting the following settings:

* CA certificate to secure the connection (```ca_cert``` setting).
* MQTT credentials for the ESP32 (```mqtt_client_user``` and ```mqtt_client_pwd``` settings)

In order to enable secure connections to the MQTT broker running on the Pi, the ESP32 needs to know the **CA certificate** created in [step 06 - security](../step06-security/).
See this [tutorial about ESP32 and MQTTS](http://www.iotsharing.com/2017/08/how-to-use-esp32-mqtts-with-mqtts-mosquitto-broker-tls-ssl.html), if you want to learn more about this topic.

To view the certificate that needs to be copied into the sketch configuration, you can log into the Pi via SSH and run the following command (adjust path and filename according to those you actually used in step 06):

```
cat ~/certs/myhomeCA/myhome.rootCA.crt
```

Example output:

```
-----BEGIN CERTIFICATE-----
ABC12345DEFGH/987654ijklmnopqrstuvwxyz012345/54321abcdefghijklmn
<... a lot more lines, truncated here for simplicity ...>
-----END CERTIFICATE-----
```

When you copy this to the sketch configuration file, note that each line from above needs to be first appended by ```\n```, then put in quotes and finally appended by ``` \``` to be valid source code. The [included example configuration file]((wifi_mqtt_part/wifi_mqtt_config.h.example)) illustrates this.

The ESP32 still needs some **credentials for the MQTT broker**. Log into the Pi again via SSH and run the following commands to create an ```esp32``` user:

```
sudo mosquitto_passwd /etc/mosquitto/passwd esp32
sudo service mosquitto restart
```

Enter these credentials in the sketch configuration file and save it.

**Note on hard-coded configuration**: The settings and credentials for the WiFi and the MQTT broker are hard-coded into the program here. This approach is based on the assumption that the location of the ESP32 and the wireless network it is connected to rarely change. It also enables the ESP32 to seamlessly continue its operation in case power is cut - may it be intentionally (e. g. by an external time switch) or unintentionally (e. g. dead battery, no solar power). If you require a more flexible configuration, consider using the [**WiFi Manager library**](https://github.com/zhouhan0126/WIFIMANAGER-ESP32) as explained e. g. by [this tutorial](https://www.instructables.com/id/Avoid-Hard-Coding-WiFi-Credentials-on-Your-ESP8266/).

#### Run the sketch

Open a serial monitor (__Tools__ -> __Serial Monitor__) and set it to 115200 baud.
Upload the program (__Sketch__ -> __Upload__) and watch the output in the log window at the bottom. Once the program starts, you should see the following (example) output on the serial monitor:

```
Connecting to WiFi: My WiFi SSID
WiFi connected, IP address: 192.168.1.110
Connected to MQTT broker: mypi
Publishing to /test - Done
Publishing to /test - Done
...
```

#### Troubleshooting

In case, the **connection to the WiFi** fails, double check the configuration and try rebooting / resetting your ESP32.

In case, the **connection to the MQTT broker** fails, the program will output a response code (rc). See the [PubSubClient error codes](https://github.com/knolleary/pubsubclient/blob/master/src/PubSubClient.h#L44) for their meaning.

## Putting it all together

### Wiring

The wiring is basically the same as specified above for the individual sensors, except that two sensors need to be connected now to the same four I2C pins of the ESP32. If you use a breadboard, the vertical busses (usually labelled with + / -) are ideal to do that job. The following pictures illustrate this setup.

<a href="outdoor_unit_breadboard_front.jpg"><img src="outdoor_unit_breadboard_front.jpg" width="400"></a>
<a href="outdoor_unit_breadboard_rear.jpg"><img src="outdoor_unit_breadboard_rear.jpg" width="400"></a>

### Program

The sketch to implement the outdoor unit is provided with this guide: [outdoor_unit](outdoor_unit/). It is essentially copied and pasted from sketches created earlier (see sections above on BME280, TSL2591 and MQTT). All these parts have been put in separates file to improve the structure of the source code. A few minor changes and additions have been performed.

* [```outdoor_unit.ino```](outdoor_unit/outdoor_unit.ino): The main program with the ```setup()``` and ```loop()``` functions.
* [```bme280.ino```](outdoor_unit/bme280.ino): All code related to the BME280 sensor based on the earlier sketch above. The function ```publishBme280()``` has been added to publish the sensor data.
* [```tsl2591.ino```](outdoor_unit/tsl2591.ino): All code related to the TSL2591 sensor based on the earlier sketch above. The function ```publishTsl2591()``` has been added to publish the sensor data.
* [```wifi_mqtt.ino```](outdoor_unit/wifi_mqtt.ino): All code related to the WiFi and MQTT broker based on the earlier sketch above. The function ```publishValue(const char* topic, float value, int precision)``` has been added for convenience to publish the numeric sensor data.
* [```outdoor_unit_config.h.example```](outdoor_unit/outdoor_unit_config.h.example): This is an example configuration based on the earlier sketches above. Settings for the MQTT topics have been added. The file needs to be copied and/or renamed to ```outdoor_unit_config.h``` and adjusted to your setup prior to compiling and running the program.


Open a serial monitor (__Tools__ -> __Serial Monitor__) and set it to 115200 baud.
Make sure you adjusted the configuration file.
Then upload the program (__Sketch__ -> __Upload__) and watch the output in the log window at the bottom. Once the program starts, you should see the following (example) output on the serial monitor:

```
Connecting to WiFi: My WiFi SSID
WiFi connected, IP address: 192.168.1.110
Connected to MQTT broker: mypi
Read BME280: Temperature [C] = 26.6, Pressure [hPa] = 999, Humidity [%] = 57.6, Approx. altitude [m] = 181
Read TSL2591: Full = 7821, IR = 2781, Visible = 3813, Lux = 176
Publishing to /outdoors/temperature - Done
Publishing to /outdoors/airpressure - Done
Publishing to /outdoors/humidity - Done
Publishing to /outdoors/illumination - Done
...
```

### Case

The outdoor unit needs to be protected against wind and weather while still being able to have enough exposure to perform accurate measurements. The design of a custom case to neatly fit the hardware into is out of scope for this step. However, it may be added to this guide in the future.

In case you just want to put your breadboard setup to action, a sheltered location or building a rudimental, auxiliary case may also work for you. Here is an example of the latter made of wood, plexiglass and hot glue:

<a href="outdoor_unit_aux_case.jpg"><img src="outdoor_unit_aux_case.jpg" width="400"></a>
<a href="outdoor_unit_aux_case_plan.pdf"><img src="outdoor_unit_aux_case_plan.png" width="400"></a>

## openHAB configuration

TODO
