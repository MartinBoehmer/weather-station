# Weather station

This repo contains instructions to build a weather station with indoor and outdoor measurements as well as online forecasts. The hardware is based on a Raspberry PI for the indoor part and an ESP32 for the outdoor part.

The instructions are split in to small, self-contained steps that each produce a useful and working result. With each step taken, the functionality and maturity of the solution increases. The intention behind this is that the weather station can be iteratively built over a couple of evenings or weekends.

## Instructions

* [Step 00: Preparation](step00-preparation)
* [Step 01: Simple clock](step01-simple_clock)
* [Step 02: openHAB](step02-openhab)
* [Step 03: Indoor temperature and humidity](step03-indoor-temp)
* [Step 04: LCD dimmer](step04-lcd_dimmer)
* [Step 05: Weather forecasts](step05-forecasts)
* [Step 06: Security](step06-security)
* [Step 07: Outdoor unit](step07-outdoor_unit)

## Open points

This might be added in the future (time permitting):

* Purpose-built case for the Raspberry Pi (indoor unit)
* Purpose-built case for the ESP32 (outdoor unit)

If you want something particular to be added or enhanced, please consider submitting an issue or a pull-request.

## Copyright and license

Copyright 2020 Martin Böhmer. Licensed under the [MIT License](LICENSE).
