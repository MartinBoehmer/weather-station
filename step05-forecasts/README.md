# Weather forecasts

<a href="habpanel-preview1.PNG"><img src="habpanel-preview1.PNG" align="right" width="200" ></a>
<a href="habpanel-preview2.PNG"><img src="habpanel-preview2.PNG" align="right" width="200" ></a>

In this step, a 3-day weather forecast based on [meteoblue's widgets](https://www.meteoblue.com) will embedded into openHAB's HabPanel. As this forecast is solely intended to be "just displayed", no data binding or data processing is involved.

[[_TOC_]]

## Prerequisites

### Required parts

No other parts required than those from the [preparation step](../step00-preparation/).

### Steps to be completed prior to this step

* [Preparation](../step00-preparation/)
* [openHAB](../step02-openhab/)

## Software installation

No additional software required.

## openHAB configuration

meteoblue offers customisable weather forecast widgets that can be embedded into any website by an IFrame HTML element. openHAB's HabPanel Frame widget provides an easy way to include an IFrame HTML element in a HabPanel dashboard.

### Day-based forecast

Go to the [meteoblue Day widget configuration](https://www.meteoblue.com/en/weather/widget/setupday/hamburg_deutschland_2911298) to setup your custom widget.

You may change the language of the website and the forecast by changing the language code in your browser's address bar. Change ```https://www.meteoblue.com/en/...``` e. g. to ```https://www.meteoblue.com/de/...``` for German.

Here are the settings used in this tutorial:

| Setting            | Value                | Comment |
| ---                | ---                  | ---     |
| Location           | Use current location | Change the location to yours at the top of the page |
| Days               | 3                    | More days won't fit on the dashboard setup below |
| Temperature unit   | °C                   | Choose as you like |
| Wind speed unit    | km/h                 | Choose as you like |
| Precipitation unit | mm                   | Choose as you like |
| Coloured           | Monochrome           | This setting will make the widget seamlessly blend into the HabPanel look and feel |
| Parameters         | Pictogram, Temperature min/max, Wind speed, Wind direction, UV-index, Precipitation, Precipitation probability | You may choose parameters as you like and as space on the HabPanel dashboard permits. All won't fit. |
| Background         | No background: Bright text | Choose according to your HabPanel theme |

When you are done, the widget's HTML is output at the bottom of the page. Example:

```html
<iframe src="https://www.meteoblue.com/en/weather/widget/daily/hamburg_germany_2911298?geoloc=fixed&days=4&tempunit=CELSIUS&windunit=KILOMETER_PER_HOUR&precipunit=MILLIMETER&coloured=monochrome&pictoicon=0&maxtemperature=0&maxtemperature=1&mintemperature=0&mintemperature=1&windspeed=0&windspeed=1&windgust=0&winddirection=0&uv=0&humidity=0&precipitation=0&precipitation=1&precipitationprobability=0&spot=0&pressure=0&layout=dark"  frameborder="0" scrolling="NO" allowtransparency="true" sandbox="allow-same-origin allow-scripts allow-popups allow-popups-to-escape-sandbox" style="width: 216px; height: 220px"></iframe><div><!-- DO NOT REMOVE THIS LINK --><a href="https://www.meteoblue.com/en/weather/week/hamburg_germany_2911298?utm_source=weather_widget&utm_medium=linkus&utm_content=daily&utm_campaign=Weather%2BWidget" target="_blank">meteoblue</a></div>
```

What we are only interested in is the URL with the configuration that you made above. You'll find it in the ```src``` attribute of the ```<iframe>``` element. Copy this URL to the clipboard or a text file. Example:

```
https://www.meteoblue.com/en/weather/widget/daily/hamburg_germany_2911298?geoloc=fixed&days=4&tempunit=CELSIUS&windunit=KILOMETER_PER_HOUR&precipunit=MILLIMETER&coloured=monochrome&pictoicon=0&maxtemperature=0&maxtemperature=1&mintemperature=0&mintemperature=1&windspeed=0&windspeed=1&windgust=0&winddirection=0&uv=0&humidity=0&precipitation=0&precipitation=1&precipitationprobability=0&spot=0&pressure=0&layout=dark
```

When you change the configuration of the widget, the URL will change accordingly. So, you need to extract it again after making changes.

### 3h forecast

A more detailed forecast with 3h intervals will be displayed on a separate HabPanel dashboard.

To setup the widget for this forecast, go to the [meteoblue 3h 	widget configuration](https://www.meteoblue.com/en/weather/widget/setupthree/hamburg_germany_2911298) to setup your custom widget.

Again, you may change the language as described above for the Day-based widget.

Here are the settings used in this tutorial:

| Setting            | Value                | Comment |
| ---                | ---                  | ---     |
| Location           | Use current location | Change the location to yours at the top of the page |
| Without current weather | Tick            | Saves precious space on the HabPanel dashboard |
| Without forecast   | Don't tick           |  |
| Days               | 4                    | More days won't fit on the dashboard setup below |
| Temperature unit   | °C                   | Choose as you like |
| Wind speed unit    | km/h                 | Choose as you like |
| Background         | No background: Bright text | Choose according to your HabPanel theme |

When you are done, the widget's HTML is output at the bottom of the page. Extract the URL to your custom widget as described above for the Day-based widget.

### Visualisation

Navigate your browser to HabPanel, e. g. http://mypi/habpanel/. Click in the cog icon (upper right corner) and then on "panel settings" (on the right side).

Click "edit local panel configuration (experts only)" and replace the entire content by importing the following file: [habpanel-forecasts](habpanel-forecasts.json)

Click "Save" to update the the panel configuration or local storage (whatever is currently active, see radio buttons).

Navigate to the dashboard **Weather Overview** and switch to the edit mode (click cog next to dashboard's name at the top). Edit the settings of the frame widget labelled "Meteoblue Day widget" and replace the URL by the one that you extracted above for the Day-based meteoblue widget. Save the settings and the dashboard.

Navigate to the dashboard **Weather 3h Forecast** and switch to the edit mode (click cog next to dashboard's name at the top). Edit the settings of the frame widget labelled "Meteoblue Widget 3h" and replace the URL by the one that you extracted above for the 3h meteoblue widget. Save the settings and the dashboard.

### Auto-start configuration

To configure the auto-start of the new panel, open the file that has used for auto-start in the previous steps:

```bash
sudo nano /etc/xdg/lxsession/LXDE-pi/autostart
```

At the bottom, change the path to the chromium start script so it looks like this:

```
@/home/pi/weather-station/step05-forecasts/chromium_start.sh
```

Check, if everything works by rebooting the Pi.
