# LCD dimmer

In this step, a digital light-dependent resistor (LDR) is used to dim the brightness of the display in case it get dark around the Pi.

[[_TOC_]]

## Prerequisites

### Required parts

* Digital photoresistor (LM393, see [this tutorial](http://www.uugear.com/portfolio/using-light-sensor-module-with-raspberry-pi/))
* Jumper cables
* All parts from [preparation step](../step00-preparation/)

### Steps to be completed prior to this step

* [Preparation](../step00-preparation/)

## Assemble the parts

Wiring the sensor is simple as the sensor can be directly connected to the Pi. The physical pin number on the Pi reflects the board numbering according to the [official GPIO documentation](https://www.raspberrypi.org/documentation/usage/gpio/images/GPIO-Pinout-Diagram-2.png).

| Sensor pin (function)  | [Pi physical pin (function)](https://www.raspberrypi.org/documentation/usage/gpio/images/GPIO-Pinout-Diagram-2.png) |
| ---                    | ---              |
| AO (analog output)     | -                |
| DO (digital output)    | 15 (GPIO 22)     |
| GND (Ground)           | 20 (Ground)      |
| VCC (Power supply)     | 17 (3V3 power)   |

## Software installation

### Python3 libraries

Install the GPIO library to read the sensor data:

```bash
sudo apt-get install python3-pip
sudo pip3 install RPI.GPIO
```

## Reading and publishing the sensor data

Clone this repository to your Pi as it includes the required scripts:

```bash
cd ~
git clone https://gitlab.com/MartinBoehmer/weather-station.git
```

### Sensor check and adjustment

First, let's check, if the sensor works properly. If your wiring differs from the one specified above, you need to adjust the script before starting it.

```bash
cd ~/weather-station/step04-lcd_dimmer
python3 ldr_simpletest.py
```

When the sensor outputs **HIGH**, the light intensity is below the threshold given by the potentiometer. Hence, if the sensor output is **LOW**, the intensity is above the threshold. The script's will not produce any output as long this is the case.

While the script is running, play around with the sensor by covering it or exposing it to light. Adjust the potentiometer so the threshold fits your needs.

Sample output:

```
Input GPIO 22 HIGH 0
Input GPIO 22 HIGH 1
Input GPIO 22 HIGH 2
Input GPIO 22 HIGH 3
Input GPIO 22 HIGH 4
Input GPIO 22 HIGH 5
Input GPIO 22 HIGH 6
Input GPIO 22 HIGH 7
Input GPIO 22 HIGH 8
```

You can stop the script by pressing Ctrl+C.

### Continuous LCD dimming

The brightness is set by a value in a file that can only be written by ```root``` by default. In order to run the dimming as the user ```pi```, the permissions need to be changed. As the system resets the file permissions on every boot, it is more complicated than simply running ```chown```.

The Backlight system can be configured by [adding ```udev``` rules](https://wiki.archlinux.org/index.php/backlight#ACPI). Create or open the file:

```bash
sudo nano /etc/udev/rules.d/backlight.rules
```

And add the following lines at the very end:

```
ACTION=="add", SUBSYSTEM=="backlight", RUN+="/bin/chgrp video /sys/class/backlight/%k/brightness"
ACTION=="add", SUBSYSTEM=="backlight", RUN+="/bin/chmod g+w /sys/class/backlight/%k/brightness"
```

Close the file and reboot. The rules will allow members of the ```video``` group to change the brightness. By default ```pi``` is already a member of that group, so no need to adjust group memberships. To cross-check, ```video``` should appear, when listing the group memberships:

```bash
groups pi | grep video
```

Now check, if the dimming works by running the following script and changing the light intensity the sensor is faced. If your wiring differs from the one specified above, you need to adjust the script before starting it.

```bash
cd ~/weather-station/step04-lcd_dimmer
python3 ldr_lcd_dimmer.py
```

To run the script automatically when the Pi boots, define a new cron job in the crontab of the (current) user ```pi```:

```bash
crontab -e
```

Add the following lines at the very end of the file. Make sure the last line is a comment or an blank line as shown below:

```
# Auto-start LCD dimmer
@reboot /usr/bin/python3 /home/pi/weather-station/step04-lcd_dimmer/ldr_lcd_dimmer.py
#
```
