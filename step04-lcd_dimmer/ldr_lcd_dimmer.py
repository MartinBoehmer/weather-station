import time

import RPi.GPIO as GPIO

# Settings
LDR_PIN_BCM = 22  # = GPIO 22, data pin
LCD_BRIGHTNESS_NORMAL = 150  # LCD brightness ranges from 0-255 (255 = brightest)
LCD_BRIGHTNESS_DIMMED = 25
READ_DELAY = 1.0  # in seconds
LCD_BRIGHTNESS_FILE = "/sys/class/backlight/rpi_backlight/brightness"

# Setup GPIO
GPIO.setmode(GPIO.BCM)
GPIO.setup(LDR_PIN_BCM, GPIO.IN)

# Save current brightness
with open(LCD_BRIGHTNESS_FILE, 'r') as f:
    inital_brightness = int(f.read())

# Read and process sensor data
last_ldr_state = -1
try:
    while True:
        current_ldr_state = GPIO.input(LDR_PIN_BCM)
        if current_ldr_state != last_ldr_state:

            if GPIO.input(LDR_PIN_BCM) == GPIO.HIGH:
                # HIGH = Environment is dark
                target_brightness = LCD_BRIGHTNESS_DIMMED
            else:
                # LOW = Envorinment is bright
                target_brightness = LCD_BRIGHTNESS_NORMAL
            with open(LCD_BRIGHTNESS_FILE, 'w') as f:
                f.write(str(target_brightness))

        last_ldr_state = current_ldr_state

        time.sleep(READ_DELAY)

except KeyboardInterrupt:
    pass

finally:
    GPIO.cleanup()

# Reset initial brightness
with open(LCD_BRIGHTNESS_FILE, 'w') as f:
    f.write(str(inital_brightness))
