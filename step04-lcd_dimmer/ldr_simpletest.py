import time

import RPi.GPIO as GPIO

LDR_PIN_BCM = 22

GPIO.setmode(GPIO.BCM)
GPIO.setup(LDR_PIN_BCM, GPIO.IN)

i = 0
try:
    while True:
        if GPIO.input(LDR_PIN_BCM) == GPIO.HIGH:
            print("Input GPIO " + str(LDR_PIN_BCM) + " HIGH " + str(i))
            i += 1
        time.sleep(0.5)

except KeyboardInterrupt:
    pass

finally:
    GPIO.cleanup()
