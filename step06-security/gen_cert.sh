#!/bin/bash

set -e

# Debug output
debug=false

# Check, if config has been provided
if [ ! -f "$1" ]; then
    echo "Error: No config file specified or file does not exist!"
    echo "Usage: gen_cert <config-file>"
    exit 1
fi
cert_config="$1"
cert_config_ext="$cert_config.tmp"
cert_dir=$( dirname "$1" )
cert_dir="$cert_dir/"

# Read config
function get_config () {
    local retval=$( sed -n "s/^$1\s*=\s*\(.*\)$/\1/p" "$cert_config" | tr -d '\r' | tr -d '\n' )
    echo "$retval"
}
key_length=$( get_config 'key.length' )
key_file=$( get_config 'key.file' )
key_file="$cert_dir$key_file"
cert_file=$( get_config 'cert.file' )
cert_file="$cert_dir$cert_file"
root_cert_file=$( get_config 'root.cert.file' )
root_cert_file="$cert_dir$root_cert_file"
root_key_file=$( get_config 'root.key.file' )
root_key_file="$cert_dir$root_key_file"
cert_subj=$( get_config 'cert.subj' )
cert_days=$( get_config 'cert.days' )
csr_file="${cert_file}.csr"
cert_is_ca=$( grep 'CA:true' "$cert_config" | wc -l )
fullchain_file="$cert_file.fullchain"
combined_file="$cert_file.combined"


# What is going to happen?
if [ "$cert_is_ca" != 0 ]; then
    # Create CA
    pwd_option=" -des3"
    cert_file_check="$root_cert_file"
	key_file_check="$root_key_file"
else
    # Create CA-signed certificate
    pwd_option=""
    cert_file_check="$cert_file"
	key_file_check="$key_file"
fi
key_file_public="$key_file_check.public"

# Debug output
if [ "$debug" = true ]; then
    echo "DEBUG: Configuration:"
    echo "  Certificate directory: $cert_dir"
    echo "  Key: $key_file"
    echo "  Certificate: $cert_file"
    echo "  Root certificate: $root_cert_file"
    echo "  Root key: $root_key_file"
    echo "  CSR: $csr_file"
    echo "  CSR subj: $cert_subj"
    echo "  Generate Root CA: $cert_is_ca"
	echo "  Certificate to check: $cert_file_check"
	echo "  Key to check: $key_file_check"
	echo "  Password option: $pwd_option"
	echo "  Fullchain file: $fullchain_file"
	echo "  Combined key + fullchain file: $combined_file"
fi

# Create key, if it does not exist
if [ ! -s "$key_file_check" ]; then
    # Check for certificate without key
    if [ -f "$cert_file_check" ]; then
        echo "Error: Inconsistent state: Certificate present, but no key file!"
        echo "Certificate: $cert_file_check"
        echo "Missing key file: $key_file_check"
        exit 1
    fi
    # Generate key
	echo "No key found or key file is empty. Generating: $key_file_check"
    openssl genrsa$pwd_option -out "$key_file_check" "$key_length"
	# Generate public key
	echo "Generating public key file: $key_file_public"
	openssl rsa -in "$key_file_check" -pubout -out "$key_file_public"
	# Change permissions
	chmod go= "$key_file_check" || true
	chmod go=r "$key_file_public" || true
else
    echo "Using existing key file: $key_file_check"
fi

# Create certificate
if [ ! -s "$cert_file_check" ]; then
    echo "No certificate found or certificate file is empty."
    cat /etc/ssl/openssl.cnf "$cert_config" > "$cert_config_ext"
    if [ "$cert_is_ca" != 0 ]; then
        # Create CA root certificate
        echo "Generating Root Certificate: $root_cert_file"
        openssl req -x509 -new -nodes -sha512 -days "$cert_days" -config "$cert_config_ext" -extensions req_root_cert_ext \
          -key "$root_key_file" -out "$root_cert_file" \
          -subj "$cert_subj"
    else
        # CA-signed certificate
        # Create CSR and certificate, if certificate does not exist
        echo "Generating CSR: $csr_file"
        openssl req -new -sha512 -days "$cert_days" -config "$cert_config_ext" -reqexts req_cert_ext \
          -key "$key_file" -out "$csr_file" \
          -subj "$cert_subj"
        echo "Generating CA-singed certificate: $cert_file"
        openssl x509 -req -sha512 -days "$cert_days" -extfile "$cert_config" -extensions req_cert_ext \
          -CA "$root_cert_file" -CAkey "$root_key_file" -CAcreateserial \
          -in "$csr_file" -out "$cert_file"
        rm "$csr_file"
		# Create fullchain file
		echo "Creating file with full chain: $fullchain_file"
		cat "$cert_file" "$root_cert_file" > "$fullchain_file"
		# Create combined file
	    echo "Creating combined file with private key and fullchain: $combined_file"
	    cat "$key_file" "$fullchain_file" > "$combined_file"
		chmod go= "$combined_file" || true
    fi
    rm "$cert_config_ext"
    echo "Done."
else
    echo "Certificate found: $cert_file_check. Nothing to do..."
fi
