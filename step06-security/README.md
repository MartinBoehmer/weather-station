# Security

In this step, the services running on the Pi are secured by encrypting communication and setting up password-protections.

**Important**: This is not a guide to enterprise-strength or high-level security. It is rather intended to make a home computing environment a bit more secure. As a result, simplification are made to make it more convenient to setup and maintain. Major simplifications will be explicitly noted.

[[_TOC_]]

## Prerequisites

### Required parts

No other parts required than those from the previous steps listed below.

### Steps to be completed prior to this step

* [Preparation](../step00-preparation/)
* [openHAB](../step02-openhab/)
* [Indoor temperature and humidity](../step03-indoor-temp/)

## Software installation

No additional software required.

## Create certificates

### Create Root Certificate Authority (CA)

**Major simplification**: No [intermediate CA](https://roll.urown.net/ca/ca_intro.html#root-and-intermediate-ca) is used. See this [very detailed tutorial on setting up a root and intermediate CA](https://roll.urown.net/ca/ca_root_setup.html) in case you want to dive deeper into the topic and take more advanced security measures.

#### Automated procedure

You can use the script [```gen_cert.sh```](gen_cert.sh) from this repository. It will perform the steps according to the manual procedure described below.

Clone this repository to your Pi as it includes the required scripts:

```bash
cd ~
git clone https://gitlab.com/MartinBoehmer/weather-station.git
```

Create a configuration file by copying or editing [```myhomeCA.ssl.conf```](myhomeCA/myhomeCA.ssl.conf). Then run:

```bash
cd ~/weather-station/step06-security
./gen_cert.sh myhomeCA/myhomeCA.ssl.conf
```

The CA's private key and certificate will be located in the ```myhomeCA``` directory.

The script will not overwrite existing key or certificate files! So, if you want to re-issue the certificate and the key, you need to manually delete them first.

#### Manual procedure

Create a private key. If you do not want to password-protected the key (which you should!), leave out the ```-des3``` option. In any case you should protected the key on the file-system level as shown. You can change the prefix ```myhome``` to whatever name you like or a (fictitious) domain name.

```bash
cd ~
mkdir -p certs/myhomeCA
cd certs/myhomeCA
openssl genrsa -des3 -out myhome.rootCA.key 4096
chmod go= myhome.rootCA.key
```

Create a configuration file by copying and adjusting the example provided with this guide: [```myhomeCA.ssl.conf```](myhomeCA/myhomeCA.ssl.conf).

You can find the documentation of the settings made on the [OpenSSL X509 website](https://www.openssl.org/docs/man1.1.1/man5/x509v3_config.html).
More details on name constraints can be found [here](https://www.sysadmins.lv/blog-en/x509-name-constraints-certificate-extension-all-you-should-know.aspx) and [here](https://systemoverlord.com/2020/06/14/private-ca-with-x-509-name-constraints.html).
For details on subject alternative names (SAN), see e. g. [this site](http://wiki.cacert.org/FAQ/subjectAltName).

```bash
nano myhomeCA.ssl.conf
```

Create and self-sign the root certificate. The certificate will be valid for 3 years. Change the ```-days``` argument, if you want to change that period of time.

```bash
cat /etc/ssl/openssl.cnf myhomeCA.ssl.conf > openssl.conf.tmp
openssl req -x509 -new -nodes -sha512 -days 1096 -config openssl.conf.tmp -extensions req_root_cert_ext \
  -key myhome.rootCA.key -out myhome.rootCA.crt \
  -subj "/C=DE/ST=HH/L=Hamburg/O=Myhome/CN=Myhome Root CA"
rm openssl.conf.tmp
```

The CA's key and certificate will be located in the current directory.

### Install the root certificate

The root certificate (the crt file created above, PEM format) needs to be configured as trusted on every system that is going to communicate with your Pi and on the Pi itself.

On **Raspbian**, **Debian** and **Ubuntu** the steps are as follows. The last command is used to check, if the certificate has been installed.

```bash
sudo cp myhome.rootCA.crt /usr/local/share/ca-certificates
sudo chown root: /usr/local/share/ca-certificates/myhome.rootCA.crt
sudo dpkg-reconfigure ca-certificates
ls -lisa /etc/ssl/certs | grep myhome
```

For adding the root certificate on **Windows**, see [this tutorial, section "Manually configuring a Windows system to trust your CA"](https://www.techrepublic.com/article/how-to-add-a-trusted-certificate-authority-certificate-to-chrome-and-firefox/).

If you use the **Firefox** browser, you need to manually add the root certificate in Firefox's list of root CAs as it is separated from the one of the system. See [this tutorial, section "Manually configuring Firefox to trust your CA"](https://www.techrepublic.com/article/how-to-add-a-trusted-certificate-authority-certificate-to-chrome-and-firefox/).
Other browsers, like **Internet Explorer**, **Edge** and **Chrome** will work with the system-based certificate settings.

Instruction to import the root certificate on **Android** can be found [here](https://www.lastbreach.com/blog/importing-private-ca-certificates-in-android), for **iOS** see [this guide](https://support.apple.com/en-us/HT204477).

### Create CA-signed certificate

#### Automated procedure

Create a configuration file by copying or editing [```mypi.ssl.conf```](mypi/mypi.ssl.conf). Then run:

```bash
cd ~/weather-station/step06-security
./gen_cert.sh mypi/mypi.ssl.conf
```

The Pi's private key and certificate will be located in the ```mypi``` directory.

The script will not overwrite existing key or certificate files! So, if you want to re-issue the certificate and the key, you need to manually deleted them first.

#### Manual procedure

Create a private key:

```bash
cd ~/certs
mkdir mypi
cd mypi
openssl genrsa -out mypi.key 4096	
chmod go= mypi.key
```

Create and verify the certificate signing request (CSR):

```bash
cat /etc/ssl/openssl.cnf mypi.ssl.conf > openssl.conf.tmp
openssl req -new -sha512 -days 1096 -config openssl.conf.tmp -reqexts req_cert_ext \
  -key mypi.key -out mypi.csr \
  -subj "/C=DE/ST=HH/L=Hamburg/O=MyPi/CN=mypi"
rm openssl.conf.tmp
openssl req -in mypi.csr -noout -text
```

Sign and verify the certificate:

```bash
openssl x509 -req -sha512 -days 1096 -extfile mypi.ssl.conf -extensions req_cert_ext \
  -CA ../myhomeCA/myhome.rootCA.crt -CAkey ../myhomeCA/myhome.rootCA.key -CAcreateserial \
  -in mypi.csr -out mypi.crt
openssl x509 -in mypi.crt -text -noout
```

The Pi's private key and certificate will be located in the current directory.

### Install CA-signed certificate

Install the Pi's private key and certificate into the common locations by the following commands. The paths in this example are based on the automated generation approach.

```bash
sudo cp ~/weather-station/step06-security/mypi/mypi.crt /etc/ssl/certs
sudo cp ~/weather-station/step06-security/mypi/mypi.key /etc/ssl/private
sudo chown root:ssl-cert /etc/ssl/private/mypi.key
sudo chmod u=rw,g=r,O= /etc/ssl/private/mypi.key
```

## Secure openHAB

### Secure communication (via Nginx)

Securing the openHAB communication only affects Nginx, which is used as a proxy for openHAB in this tutorial.

If you haven't done this yet, remember to
* install the root CA's certificate as described above
* install the Pi's private key and certificate as described above

Create a new site configuration:

```
sudo nano /etc/nginx/sites-available/openhab-ssl
```

Put the following content into the file:

```
# Enforce redirection to HTTPS
#server {
#    listen 80;
#    server_name mypi mypi.myhome;
#    return 301 https://$server_name$request_uri;
#}

# HTTPS configuration
server {
    listen 443 ssl;

    server_name mypi mypi.myhome;

    ssl_certificate /etc/ssl/certs/mypi.crt;
    ssl_certificate_key /etc/ssl/private/mypi.key;
	ssl_trusted_certificate /etc/ssl/certs/myhome.rootCA.pem;
    include /etc/nginx/snippets/ssl-params.conf;

    location / {
        proxy_pass http://localhost:8080/;
        include /etc/nginx/proxy_params;
    }
}
```

Create the SSL parameters file according to this [tutorial](https://www.digitalocean.com/community/tutorials/how-to-create-a-self-signed-ssl-certificate-for-nginx-on-debian-8):

```
sudo nano /etc/nginx/snippets/ssl-params.conf
```

Content is as follows:

```
# from https://cipherli.st/
# and https://raymii.org/s/tutorials/Strong_SSL_Security_On_nginx.html

ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
ssl_prefer_server_ciphers on;
ssl_ciphers "EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH";
ssl_ecdh_curve secp384r1;
ssl_session_cache shared:SSL:10m;
ssl_session_tickets off;
ssl_stapling on;
ssl_stapling_verify on;
resolver 8.8.8.8 8.8.4.4 valid=300s;
resolver_timeout 5s;
# Disable preloading HSTS for now.  You can use the commented out header line that includes
# the "preload" directive if you understand the implications.
#add_header Strict-Transport-Security "max-age=63072000; includeSubdomains; preload";
add_header Strict-Transport-Security "max-age=63072000; includeSubdomains";
add_header X-Frame-Options DENY;
add_header X-Content-Type-Options nosniff;

ssl_dhparam /etc/ssl/certs/dhparam.pem;
```

Enable the site and check the configuration:

```bash
sudo ln -s /etc/nginx/sites-available/openhab-ssl /etc/nginx/sites-enabled/
sudo nginx -t
sudo systemctl restart nginx
```

If there is a warning about "ssl_stapling", you can safely ignore it as it, because an OCSP responder URL is not required for a home network (another simplification).

openHAB should now be accessible, e. g. via https://mypi/.

If you want to enforce the redirection from HTTP to HTTPS disable the previously defined site for HTTP by removing the symbolic link:

```bash
sudo rm /etc/nginx/sites-enabled/openhab
```

Uncomment the respective server configuration in the HTTP site:

```bash
sudo nano /etc/nginx/sites-available/openhab-ssl
```

Reload Nginx:

```bash
sudo nginx -t
sudo systemctl restart nginx
```

### Authentication (via Nginx)

Install the ```htpasswd``` tool:

```bash
sudo apt-get install apache2-utils
```

Create a password file with a first user and restrict access to the file to the Mosquitto group.

```bash
sudo htpasswd -c /etc/nginx/conf.d/.htpasswd openhab
sudo chown www-data: /etc/nginx/conf.d/.htpasswd
sudo chmod o= /etc/nginx/conf.d/.htpasswd
```

You can add additional users by omitting the ```-c``` parameter and delete users employing the ```-D``` parameter (see [Apache HTTP Server documentation](https://httpd.apache.org/docs/2.4/programs/htpasswd.html)).

Here, another account is added. You can change ```user1``` to whatever login name you like.

```bash
sudo htpasswd /etc/nginx/conf.d/.htpasswd user1
```

Open the openHAB SSL site configuration:

```bash
sudo nano /etc/nginx/sites-available/openhab-ssl
```

Add the following lines __after the location block__ to require authentication from all machines except the localhost.

```
    location {
        ...
    }

    satisfy any;

    allow 127.0.0.1;
    deny all;

    auth_basic "openHAB";
    auth_basic_user_file /etc/nginx/conf.d/.htpasswd;
```

Restart Nginx:

```bash
sudo nginx -t
sudo service nginx restart
```

For further information about authentication see the [Nginx documentation](https://docs.nginx.com/nginx/admin-guide/security-controls/configuring-http-basic-authentication/) or this [tutorial](https://www.tecmint.com/setup-nginx-basic-http-authentication/).

## Secure MQTT

### Secure communication

Up to now, Mosquitto's default configuration has been used. So now, a configuration file is need for customising the settings.

```bash
sudo nano /etc/mosquitto/conf.d/mypi.conf
```

Put in the following content:

```
# Allow unencrypted connections only from localhost
listener 1883 localhost

# Secure connections
listener 8883
certfile /etc/ssl/certs/mypi.crt
keyfile /etc/ssl/private/mypi.key
capath /etc/ssl/certs

# Secure websockets (optional)
listener 8083
protocol websockets
certfile /etc/ssl/certs/mypi.crt
keyfile /etc/ssl/private/mypi.key
capath /etc/ssl/certs

```

Restart Mosquitto and open the port on the firewall. Port ```8083``` needs to be open only if you enabled websockets above.

```bash
sudo service mosquitto restart
sudo ufw allow 8883/tcp
sudo ufw allow 8083/tcp
```

### Authentication

Create a password file with a first user and restrict access to the file to the Mosquitto group.

```bash
sudo mosquitto_passwd -c /etc/mosquitto/passwd openhab
sudo chown root:mosquitto /etc/mosquitto/passwd
sudo chmod o= /etc/mosquitto/passwd
```

You can add additional users by omitting the ```-c``` parameter and delete users employing the ```-D``` parameter (see [Mosquitto documentation](https://mosquitto.org/man/mosquitto_passwd-1.html)).

Here, another account is added. You can change ```mypi``` to whatever login name you like.

```bash
sudo mosquitto_passwd /etc/mosquitto/passwd mypi
```

Open the Mosquitto configuration:

```bash
sudo nano /etc/mosquitto/conf.d/mypi.conf
```

Insert the following lines at the __very beginning__ of the file:

```
allow_anonymous false
password_file /etc/mosquitto/passwd
```

With this setup, a login is strictly required and every logged in user can do basically everything. Fine-grained access rights can be defined by an [acl file](https://mosquitto.org/man/mosquitto-conf-5.html). **Simplification**: This is not covered here, but e. g. in this [tutorial](https://jaimyn.com.au/mqtt-use-acls-multiple-user-accounts/).

## Secure communication with other network devices

You may have additional devices in your network, like a router and a NAS, that you want to secure. Create a private key and a certificate for each of these devices according to the instructions above for securing the Nginx communication. You can and install them on the device by using their respective administration interfaces.

Some devices support the upload of the individual files, some other devices require a single file that contains the private key, the certificate and the root certificate. 

You can create such a file by merging the individual files, here ```device.key``` and ```device.crt``` and the root certificate, [in a particular order](https://www.digicert.com/kb/ssl-support/pem-ssl-creation.htm):

```bash
cat device.key >> device.combined.crt
cat device.crt >> device.combined.crt
cat ../myhomeCA/myhome.rootCA.crt >> device.combined.crt
chmod go= device.combined.crt
```
