import importlib
import os
import sys
import time

import adafruit_dht
import board
import digitalio
import paho.mqtt.publish as publish

# Default settings
cfg = {
    'DHT_PIN_ADA' : board.D4, # DHT data pin, here: RPI GPIO 4
    'DHT_MAX_READ_ATTEMPTS' : 3, # How many times should we attempt to read the sensor until we finally fail
    'DHT_ATTEMPT_DELAY' : 3.0, # seconds between read attempts
    'MQTT_BROKER_HOST' : "localhost",
    'MQTT_BROKER_PORT' : 1883,
    'MQTT_TOPIC_TEMPERATURE' : "/indoors/room1/temperature",
    'MQTT_TOPIC_HUMIDITY' : "/indoors/room1/humidity",
    'MQTT_CLIENT_ID' : "room1_dht",
    'MQTT_AUTH' : None,
    'MQTT_TLS' : None
}
# Load external settings, if present
config_module_name = os.path.splitext(os.path.basename(__file__))[0] + "_config"
try:
    config_module = importlib.import_module(config_module_name)
    cfg_ext = getattr(config_module, 'cfg')
    cfg.update(cfg_ext)
except ImportError:
    pass

# GPIO setup - use the internal pull-up resistor
dhtDataPin = digitalio.DigitalInOut(cfg['DHT_PIN_ADA'])
dhtDataPin.direction = digitalio.Direction.INPUT
dhtDataPin.pull = digitalio.Pull.UP

# Read the sensor data
dhtDevice = adafruit_dht.DHT22(cfg['DHT_PIN_ADA'])
temperature = 0.0
humidity = 0.0
read_attempts = 1
while read_attempts <= cfg['DHT_MAX_READ_ATTEMPTS']:
    try:
        temperature = dhtDevice.temperature
        humidity = dhtDevice.humidity
        break

    except RuntimeError as error:
        print('Error reading sensor: ' + error.args[0], file=sys.stderr)
        read_attempts += 1
        time.sleep(cfg['DHT_ATTEMPT_DELAY'])

# GPIO cleanup
dhtDataPin.deinit()

if read_attempts >= cfg['DHT_MAX_READ_ATTEMPTS']:
    print('Failed to read sensor. Tried ' + str(cfg['DHT_MAX_READ_ATTEMPTS']) + ' times. Giving up')
    sys.exit(1)
else:
    print('Read temperature: ' + str(temperature) + ', humidity: ' + str(humidity))

# Publish the data
mqtt_msgs = [(cfg['MQTT_TOPIC_TEMPERATURE'], temperature, 0, False),
             (cfg['MQTT_TOPIC_HUMIDITY'], humidity, 0, False)]
publish.multiple(mqtt_msgs, hostname=cfg['MQTT_BROKER_HOST'], port=cfg['MQTT_BROKER_PORT'],
                 client_id=cfg['MQTT_CLIENT_ID'], auth=cfg['MQTT_AUTH'], tls=cfg['MQTT_TLS'])

print("Data published to MQTT broker: " + cfg['MQTT_BROKER_HOST'])
