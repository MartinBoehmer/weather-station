import board

cfg = {
    'DHT_PIN_ADA' : board.D4, # DHT data pin, here: RPI GPIO 4
    'DHT_MAX_READ_ATTEMPTS' : 3, # How many times should we attempt to read the sensor until we finally fail
    'DHT_ATTEMPT_DELAY' : 3.0, # seconds between read attempts
    'MQTT_BROKER_HOST' : "localhost",
    'MQTT_BROKER_PORT' : 1883,
    'MQTT_TOPIC_TEMPERATURE' : "/indoors/room1/temperature",
    'MQTT_TOPIC_HUMIDITY' : "/indoors/room1/humidity",
    'MQTT_CLIENT_ID': "room1_dht",
    'MQTT_AUTH': None,
    #'MQTT_AUTH': {'username': 'mypi', 'password': 'secret'},
    'MQTT_TLS': None
    #'MQTT_TLS': {'ca_certs': '/etc/ssl/certs/mypi.pem'}
}