# Copyright (c) 2017 Mike McWethy for Adafruit Industries
# https://github.com/adafruit/Adafruit_CircuitPython_DHT/blob/master/examples/dht_simpletest.py
# Originally licensed under The MIT License (MIT)
# https://github.com/adafruit/Adafruit_CircuitPython_DHT/blob/master/LICENSE

# Modifications (c) 2020 Martin Böhmer
# Licensed under the MIT License
# https://gitlab.com/MartinBoehmer/weather-station/blob/master/LICENSE

import time

import adafruit_dht
import board
import digitalio

# Settings
DHT_PIN_ADA = board.D4 # RPI GPIO 4
READ_DELAY = 3.0  # in seconds, see max. sampling rate of sensor

# Use the internal pull-up resistor of the Raspberry Pi
dhtDataPin = digitalio.DigitalInOut(DHT_PIN_ADA)
dhtDataPin.direction = digitalio.Direction.INPUT
dhtDataPin.pull = digitalio.Pull.UP

# Initial the dht device, with data pin connected to:
dhtDevice = adafruit_dht.DHT22(DHT_PIN_ADA)

read_delay2 = 0.0
while True:
    try:
        time.sleep(read_delay2)
        read_delay2 = READ_DELAY
        # Print the values to the serial port
        temperature_c = dhtDevice.temperature
        temperature_f = temperature_c * (9 / 5) + 32
        humidity = dhtDevice.humidity
        print(
            "Temp: {:.1f} F / {:.1f} C    Humidity: {}% ".format(
                temperature_f, temperature_c, humidity
            )
        )

    except KeyboardInterrupt:
        break

    except RuntimeError as error:
        # Errors happen fairly often, DHT's are hard to read, just keep going
        print(error.args[0])

# Cleanup
dhtDataPin.deinit()
