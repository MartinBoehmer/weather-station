# Indoor temperature and humidity

<a href="habpanel-preview.PNG"><img src="habpanel-preview.PNG" align="right" width="200" ></a>

In this step, the indoor temperature and humidity are read from a sensor connected to the Pi, published to an MQTT broker and displayed in openHAB.

[[_TOC_]]

## Prerequisites

### Required parts

* DHT22 sensor (to measure temperature and humidity)
* Jumper cables
* All parts from [preparation step](../step00-preparation/)

### Steps to be completed prior to this step

* [Preparation](../step00-preparation/)
* [openHAB](../step02-openhab/)

## Assemble the parts

Wiring the sensor is simple as the sensor can be directly connected to the Pi. The physical pin number on the Pi reflects the board numbering according to the [official GPIO documentation](https://www.raspberrypi.org/documentation/usage/gpio/images/GPIO-Pinout-Diagram-2.png).

| Sensor pin (function) | [Pi physical pin (function)](https://www.raspberrypi.org/documentation/usage/gpio/images/GPIO-Pinout-Diagram-2.png) |
| ---                   | ---              |
| 1 (VCC)               | 1 (3V3 power)    |
| 2 (data)              | 7 (GPIO 4)       |
| 3 (unused)            | -                |
| 4 (GND)               | 14 (Ground)      |

The DHT22 needs to be used with a pull-up resistor to provide a reliable signal. Here, the Pi's built-in pull-up resistors (actived by software), so there is no need to consider it for the wiring. In case you want a setup with an external/separate pull-up resistor, check out [this tutorial](https://pimylifeup.com/raspberry-pi-humidity-sensor-dht22/). The wiring specified above can be kept.

## Software installation

### Adafruit Python3 libraries

The [Adafruit Python DHT library (aka. Adafruit_DHT)](https://github.com/adafruit/Adafruit_Python_DHT) is very popular to connect the the DHT22 sensor, but it is **deprecated** by now. Hence, this tutorial is based on the [Adafruit CircuitPython DHT libraries](https://github.com/adafruit/Adafruit_CircuitPython_DHT), which are still maintained (at the time of writing). If you are interested in further details about this library, check out this [Adafruit tutorial](https://learn.adafruit.com/dht-humidity-sensing-on-raspberry-pi-with-gdocs-logging/python-setup).

Install the CircuitPython DHT libraries:

```bash
sudo apt-get install python3-pip
sudo pip3 install adafruit-circuitpython-dht
sudo apt-get install libgpiod2
```

### MQTT Broker

The values read from the sensor will be published to the Mosquitto MQTT broker. Additional information can be found in [this tutorial](https://appcodelabs.com/introduction-to-iot-build-an-mqtt-server-using-raspberry-pi).

```bash
sudo apt-get install mosquitto mosquitto-clients
```

In addition, a Python MQTT library is needed:

```bash
sudo pip3 install paho-mqtt
```

## Reading and publishing the sensor data

Clone this repository to your Pi as it includes the required scripts:

```bash
cd ~
git clone https://gitlab.com/MartinBoehmer/weather-station.git
```

### Sensor check

First, let's check, if the sensor works properly. If your wiring differs from the one specified above, you need to adjust the script before starting it.

```bash
cd ~/weather-station/step03-indoor-temp
python3 dht_simpletest.py
```

Sample output:

```
Temp: 76.3 F / 24.6 C    Humidity: 48.0%
Temp: 75.9 F / 24.4 C    Humidity: 46.3%
Temp: 75.7 F / 24.3 C    Humidity: 46.3%
Temp: 75.7 F / 24.3 C    Humidity: 46.3%
Temp: 75.7 F / 24.3 C    Humidity: 46.3%
Temp: 75.7 F / 24.3 C    Humidity: 46.3%
Temp: 75.7 F / 24.3 C    Humidity: 46.3%
Temp: 75.7 F / 24.3 C    Humidity: 46.3%
Temp: 75.7 F / 24.3 C    Humidity: 46.3%
Temp: 75.7 F / 24.3 C    Humidity: 46.3%
Checksum did not validate. Try again.
Temp: 75.7 F / 24.3 C    Humidity: 46.3%
Temp: 75.7 F / 24.3 C    Humidity: 46.4%
Temp: 75.7 F / 24.3 C    Humidity: 46.4%
Temp: 75.7 F / 24.3 C    Humidity: 46.4%
Temp: 75.7 F / 24.3 C    Humidity: 46.4%
```

The checksum error is okay, as long as it does not appear too often.

### MQTT check

Next, check if the sensor data can be made available via MQTT. Open a separate command line terminal or SSH shell and subscribe all topics:

```bash
mosquitto_sub -d -t /#
```

Switch to a different command line terminal or SSH shell.

Manually publish some random data:

```bash
mosquitto_pub -d -t /indoors/room1/temperature -m "24.3"
```

This data should then appear then in your other terminal/shell running the subscription client.

Run the script that will read the actual sensor data and publish it to the MQTT broker:

```bash
cd ~/weather-station/step03-indoor-temp
python3 dht_mqtt_pub.py
```

### Continuous publication

You may want to change the topics for publishing temperature and humidity. To change them, edit the script and change the values of the variables ```MQTT_TOPIC_TEMPERATURE``` and ```MQTT_TOPIC_HUMIDITY```:

```bash
nano dht_mqtt_pub.py
```

Close, exit and run the script to verify the changes.

To run the script periodically, define a new cron job in the crontab of the (current) user ```pi```:

```bash
crontab -e
```

Add the following lines at the very end of the file. Make sure the last line is a comment or an blank line as shown below:

```
# Read DHT sensor
* *    *   *   * /usr/bin/python3 /home/pi/weather-station/step03-indoor-temp/dht_mqtt_pub.py
#
```

This will read and publish the sensor data every minute. In case you want to change the schedule, you may find the [crontab.guru tool](https://crontab.guru/#*_*_*_*_*) useful to work out the right cron expression.

## openHAB configuration

### Data binding

Add the **MQTT binding** by opening:

```bash
sudo nano /etc/openhab2/services/addons.cfg
```

Uncomment and set the following settings:

```
binding = mqtt
```

Create the **things** to query the MQTT broker by creating the file...

```bash
sudo nano /etc/openhab2/things/mypi.things
```

... and put in the following content:

```
Bridge mqtt:broker:localmqtt "Local MQTT Broker" [ host = "localhost" ] {
    Thing topic room1 "Room 1" {
        Channels:
            Type number : temperature [ stateTopic="/indoors/room1/temperature" ]
            Type number : humidity [ stateTopic="/indoors/room1/humidity" ]
    }
}
```

Create the **items** for the temperature and humidity by creating the file...

```bash
sudo nano /etc/openhab2/items/mypi.items
```

... and put in the following content:

```
Number Room1Temperature "Room 1 Temperature" <temperature> {channel="mqtt:topic:localmqtt:room1:temperature"}
Number Room1Humidity "Room 1 Humidity" <humidity> {channel="mqtt:topic:localmqtt:room1:humidity"}
```

Save and exit.

### Restore data after restart

As the sensor data is queried every minute, there is no urge for persisting it to restore it when openHAB or the Pi restarts. However, in other cases it can be useful to have this feature up and running.

This step only covers basic means provided by the [mapdb](https://www.openhab.org/addons/persistence/mapdb/) add-on. More advanced persistence approaches will be subject of a separate step as they require more effort to setup.

Create the following file...

```bash
sudo nano /etc/openhab2/persistence/mapdb.persist
```

... with this content:

```
Strategies {
  default = everyChange
}

Items {
  Room1Temperature, Room1Humidity : strategy = everyChange, restoreOnStartup
}
```

This will save the values of the two items whenever their values changes and restore the last saved values after a restart of openHAB.

### Visualisation

Navigate your browser to HabPanel, e. g. http://mypi/habpanel/. Click in the cog icon (upper right corner) and then on "panel settings" (on the right side).

Click "edit local panel configuration (experts only)" and replace the entire content by importing the following file: [habpanel-indoor-temp-hum.json](habpanel-indoor-temp-hum.json)

Click "Save" to update the the panel configuration or local storage (whatever is currently active, see radio buttons).

### Auto-start configuration

To configure the auto-start of the new panel, open the file that has used for auto-start in the previous steps:

```bash
sudo nano /etc/xdg/lxsession/LXDE-pi/autostart
```

At the bottom, change the path to the chromium start script so it looks like this:

```
@/home/pi/weather-station/step03-indoor-temp/chromium_start.sh
```

Check, if everything works by rebooting the Pi.
