#!/bin/bash

set -e

script_dir=$( dirname "$0" )

# Delay
sleep 15

# Chromium autostart 
chromium-browser --incognito --kiosk "$script_dir/clock/clock.html"