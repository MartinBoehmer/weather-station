# Simple Clock

In this step, a simple digital clock app is setup. It will start itsself when the Pi is booted in fullscreen mode.

No internet connection is required for running the clock app as all files will be kept locally.

[[_TOC_]]

## Prerequisites

### Required parts

No other parts required than those from the [preparation step](../step00-preparation/).

### Steps to be completed prior to this step

* [Preparation](../step00-preparation/)

## Software installation

To hide the mouse pointer, we need unclutter:

```bash
sudo apt-get install unclutter
```

Clone this repository to your Pi as it includes the clock app:

```bash
cd ~
git clone https://gitlab.com/MartinBoehmer/weather-station.git
```

## Auto-start configuration

Now we need to configure the auto-start of the clock app.

```bash
sudo nano /etc/xdg/lxsession/LXDE-pi/autostart
```

The contents of the file should look as follows:

```
@lxpanel --profile LXDE-pi
@pcmanfm --desktop --profile LXDE-pi
# Disable screensaver
#@xscreensaver -no-splash

# Disable power saving and blank screen
@xset s off
@xset -dpms
@xset s noblank

# Hide mouse pointer
@unclutter

# Start chromium in kiosk mode
@/home/pi/weather-station/step01-simple_clock/chromium_start.sh
```

For more details and troubleshooting on auto-start in Raspbian, see [this website](https://www.raspberrypi-spy.co.uk/2014/05/how-to-autostart-apps-in-rasbian-lxde-desktop/).

## Optional: Changing the clock's language and format

The language and format of the clock can be easily changed.

To change the **language**, change [this line in ```clock.html```](clock/clock.html#L11) to your locale (e.g. en, fr, es, ...):
```javascript
moment.locale('de');
```

In case you want to adjust the **format** of the date and time shown, change [the these lines in ```clock.html```](clock/clock.html#L14) according to the [Moment.js documentation](https://momentjs.com/docs/#/displaying/format/).
```javascript
$('#time').text(moment().format('HH:mm:ss'));
$('#date').text(moment().format('dddd, DD.MM.yyyy'));
```
