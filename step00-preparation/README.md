# Preparation

In this step, basic hardware and software installations are performed, so the Raspberry Pi is ready to use.

[[_TOC_]]

## Prerequisites

### Required parts

* Raspberry Pi 4B
* Raspberry Pi Touch Display (7" screen)
* Micro USB power supply with 5V / 3A output
* microSD card with at least 32 GB capacity
* Case for the Raspberry Pi and the display
 
For the initial setup of the Pi, you additionally need

* USB keyboard
* optional: USB mouse

### Steps to be completed prior to this step

None.

## Prepare SD card

* Download the latest [Raspbian with desktop](https://www.raspberrypi.org/downloads/raspbian/) image (Rasbian Buster)
* Copy image to SD card, e. g. using [Etcher (Windows/Mac/Linux)](https://www.balena.io/etcher/) or [Win32DiskImager (Windows)](https://sourceforge.net/projects/win32diskimager/files/Archive/win32diskimager-1.0.0-install.exe/download)
* Put SD card into Raspberry Pi

## Assemble Raspberry Pi

* Install the display as specified by [the official documentation](https://www.raspberrypi.org/documentation/hardware/display/README.md)
* Ensure the prepared SD card is properly plugged into the Pi
* Put the Pi with the mounted display into the case
* Connect the power supply to the power connector of the display (NOT the one of the Pi)
* Power on the Pi

## Setup Raspbian

### Basic setup

When your boot your PI for the first time, a setup wizard is shown (see [tutorial for screenshots](https://www.tomshardware.com/reviews/raspberry-pi-set-up-how-to,6029.html#configuring-raspbian)). Perform the following actions:

* Choose your country, language and timezone
* Set a password
* Connect the Pi to your Wi-Fi
* Update Raspbian and the software packages

Depending on your case, you may need to **flip the display orientation** by setting ```lcd_rotate=2``` in ```/boot/config.txt```, see [the official documentation](https://www.raspberrypi.org/documentation/hardware/display/README.md)

### Hostname

Set the name of your Pi used to identify it on your local network: Raspberry icon -> Preferences -> Raspberry Pi Configuration -> System -> Hostname. Reboot your Pi afterwards.

In this tutorial, ```mypi``` will be used as a hostname.

### Remote access

Allow **SSH** connections: Raspberry icon -> Preferences -> Raspberry Pi Configuration -> Interfaces -> SSH.

Optionally, you can also setup **remote desktop access**:

```bash
sudo apt-get update
sudo apt-get install xrdp
```

### Optional: Touch screen keyboard

This can be handy when you do not have the USB keyboard plugged in any more and remote access does not work.

```bash
sudo apt-get update
sudo apt-get install matchbox-keyboard
```

### Optional: Wi-Fi time switch

In case you want to switch off the Wi-Fi at night, you can do so by editing crontab. Other ways to (permanently) disable the Wi-Fi are listed by [this tutorial](https://raspberrytips.com/disable-wifi-raspberry-pi/).

```bash
sudo crontab -e
```

If you run this command for the first time, you need to choose the crontab editor first, e. g. Nano.

Add the following lines to the crontab. this will switch off the Wi-Fi at 22:45 and turn it back on at 6:40 the next morning.
```
45 22  * * *          /sbin/ifconfig wlan0 down && /usr/sbin/rfkill block wifi
40  6  * * *          /usr/sbin/rfkill unblock wifi && /sbin/ifconfig wlan0 up
```

### Optional: Turn off Bluetooth

As we do not need Bluetooth for now, it can be disabled. See [here](https://scribles.net/disabling-bluetooth-on-raspberry-pi/) for more details.

Stop and disable the services related to Bluetooth:

```bash
sudo service hciuart stop
sudo systemctl disable hciuart
sudo service bluealsa stop
sudo systemctl disable bluealsa
sudo service bluetooth stop
sudo systemctl disable bluetooth
```

Disable kernel modules (temporarily):

```bash
sudo modprobe -r hci_uart
sudo modprobe -r btbcm
```

Permanently disable the Bluetooth kernel module (see [firmware readme](https://github.com/raspberrypi/firmware/blob/master/boot/overlays/README)):

```bash
sudo nano /boot/config.txt
```

Add the following line at the end, save and exit the editor.

```
dtoverlay=disable-bt
```

To re-enable Bluetooth again, just remove or comment the line from ```/boot/config.txt``` and enable the Bluetooth services by the commands above with "start" instead of "stop" and "enable" instead of "disable".