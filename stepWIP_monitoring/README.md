# Monitoring (this is Work in Progress!)

This step adds monitoring capabilities to the outdoor unit, so you know when it is does not send data any more.

[[_TOC_]]

## Prerequisites

### Required parts

No other parts required than those from the previous steps listed below.

### Steps to be completed prior to this step

* [Preparation](../step00-preparation/)
* [openHAB](../step02-openhab/)
* [Outdoor sensor unit](../step07-outdoor_unit/)

## Item state expiry 

### Data binding

**Important**: The additional binding to enable the ```expire`` attribute in the item configuration is only required with OpenHab 2.x (an prior). OpenHab 3 supports it out-of-the-box and this section can be skipped.

Add the **[Expire binding (```expire1```)](https://v2.openhab.org/addons/bindings/expire1/)** by opening:

```bash
sudo nano /etc/openhab2/services/addons.cfg
```

Set the following settings:

```
binding = mqtt, expire1
```

### Item configuration

Open the item configuration file...

```bash
sudo nano /etc/openhab2/items/mypi.items
```

... and add the ```expire``` attribute to each item. The following examples will reset the item state to ```UnDefType.UNDEF``` or `0.0` if the items have not received an update within 5 minutes. See the [add-on documentation](https://v2.openhab.org/addons/bindings/expire1/) for more examples.

```
Number Room1Temperature "Room 1 Temperature" <temperature> {channel="mqtt:topic:localmqtt:room1:temperature", expire="5m"}
Number Room1Humidity "Room 1 Humidity" <humidity> {channel="mqtt:topic:localmqtt:room1:humidity", expire="5m, 0.0"}
```

Save and exit.

## WIP - Todo:
* Indicate expired item state in GUI
* Send alerts, see https://community.openhab.org/t/howto-get-timedate-of-last-update-of-item-in-rules/27771/5
* Use MQTT (last will) to improve device status tracking
